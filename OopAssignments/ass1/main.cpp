//Mohamed Elsaid Kamal Abdlebay
//WEACA7
#include <iostream>
#include <vector>
#include "mat.h"
#include "menu.h"

using namespace std;

#define NORMAL_MODE
#ifdef NORMAL_MODE

int main(){
    Menu menu;
    menu.run();
    return 0;
}

#else
#define CATCH_CONFIG_MAIN
#include "mat.h"
#include "catch.hpp"


TEST_CASE("setting a matrix using vector", ""){
    //Mat b
    vector<int> a;
    a.push_back(1);
    a.push_back(2);
    a.push_back(3);
    a.push_back(4);

    Mat b(a);
    CHECK(b(0,0) == 1);
    CHECK(b(1,0) == 0);
    CHECK(b(0,1) == 0);
    CHECK(b(1,1) == 2);
    CHECK(b(1,2) == 0);
    CHECK(b(2,1) == 0);
    CHECK(b(2,2) == 3);
    CHECK(b(3,2) == 0);
    CHECK(b(2,3) == 0);
    CHECK(b(3,3) == 4);
}
TEST_CASE("getting an element", ""){
    //Mat b
    Mat b({1,2,3,4});
    CHECK(b(0,0) == 1);
    CHECK(b(1,0) == 0);
    CHECK(b(0,1) == 0);
    CHECK(b(1,1) == 2);
    CHECK(b(1,2) == 0);
    CHECK(b(2,1) == 0);
    CHECK(b(2,2) == 3);
    CHECK(b(3,2) == 0);
    CHECK(b(2,3) == 0);
    CHECK(b(3,3) == 4);
}

TEST_CASE("add matrices",""){
    Mat a({1,2,3,4});
    Mat b({1,2,3,4});
    Mat c(4);
    c = a + b;
    // checking non-zero elements
    CHECK(c(0,0) == 2);
    CHECK(c(1,1) == 4);
    CHECK(c(2,2) == 6);
    CHECK(c(3,3) == 8);
    // checking some random zero element that i != j
    CHECK(c(2,3) == 0);
}

TEST_CASE("multiply matrices",""){
    Mat a({1,2,3,4});
    Mat b({1,2,3,4});
    Mat c(4);
    c = a*b;
    // checking non-zero elements
    CHECK(c(0,0) == 1);
    CHECK(c(1,1) == 4);
    CHECK(c(2,2) == 9);
    CHECK(c(3,3) == 16);
    // checking some random zero element that i != j
    CHECK(c(2,3) == 0);
}
TEST_CASE("Exceptions", ""){
    try{
        Mat a(-1);
    }catch(Mat::Exc a){
        CHECK(a  == Mat::INCORRECTSIZE);
    }

    try{
        Mat a(-1);
    }catch(Mat::Exc a){
        CHECK(a  == Mat::INCORRECTSIZE);
    }

    try{
        Mat a({1,1,1,1});
        Mat b({1,1,1});
        Mat c(4);
        c = a * b;
    }catch(Mat::Exc a){
        CHECK(a  == Mat::DIFFERENTSIZES);
    }

    try{
        Mat a({1,1,1,1});
        Mat b({1,1,1});
        Mat c(4);
        c = a + b;
    }catch(Mat::Exc a){
        CHECK(a  == Mat::DIFFERENTSIZES);
    }

    try{
        Mat a({1,1,1,1});
        a(9,9);
    }catch(Mat::Exc a){
        CHECK(a  == Mat::WRONGINDEX);
    }
}

#endif
